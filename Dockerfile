FROM python:3.6
ENV PYTHONUNBUFFERED 1

RUN mkdir /home/app \
 && mkdir -p /var/logs/ \
 && mkdir -p /var/restaraunt/ \
 && mkdir -p /var/restaraunt/logs/ \
 && mkdir -p /var/restaraunt/static/ \
 && mkdir -p /var/restaraunt/media/
WORKDIR /home/app

ADD requirements.txt .
RUN pip install -r requirements.txt

COPY . .
