from django.apps import AppConfig


class DishesConfig(AppConfig):
    name = 'restaurant.dishes'
