from functools import reduce

from django.contrib.auth import get_user_model
from django.db import models
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from restaurant.common.utils import get_formatted_date


User = get_user_model()


class Restaurant(models.Model):
    name = models.CharField('Название', max_length=200)
    city = models.CharField('Город', max_length=200)

    class Meta:
        verbose_name = 'Ресторан'
        verbose_name_plural = 'Рестораны'

    def __str__(self):
        return f'{self.name}'

    def serialize(self):
        return {
            'id': self.pk,
            'name': self.name,
            'city': self.city,
        }


class Category(MPTTModel):
    name = models.CharField('Название', max_length=200)
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        db_index=True,
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name

    def serialize(self):
        return {
            'id': self.pk,
            'name': self.name,
            'parent': self.parent.pk if self.parent else None,
            'dishes': list(map(lambda dish: dish.serialize(), self.dishes.all()))
        }

    # Красивая сериализация деревьев
    # ИЗ-ЗА ЭТИХ МЕТОДОВ ПРОИСХОДИТ МНОЖЕСТВО ЗАПРСОВО В БД
    def serialize_category_tree(self):
        data = self.serialize()
        if not self.is_leaf_node():
            data['sub_categories'] = [
                category.serialize_category_tree()
                for category in self.children.all().prefetch_related('dishes')
            ]
        return data

    @staticmethod
    def serialize_whole_tree():
        arr = []
        for category in Category.objects.filter(parent__isnull=True):
            arr.append(category.serialize_category_tree())
        return arr


class Dish(models.Model):
    name = models.CharField('Название', max_length=200)
    price = models.DecimalField('Цена', max_digits=8, decimal_places=2)
    category = models.ForeignKey(
        'Category',
        related_name='dishes',
        on_delete=models.CASCADE,
        verbose_name='Категория',
    )

    class Meta:
        verbose_name = 'Блюдо'
        verbose_name_plural = 'Блюда'

    def __str__(self):
        return self.name

    def serialize(self):
        return {
            'id': self.pk,
            'name': self.name,
            'price': str(self.price),
            'category': self.category.pk
        }


class Order(models.Model):
    PAID = 'PAID'
    CANCELED = 'CANCELED'
    PENDING = 'PENDING'

    STATUS_CHOICES = (
        (PAID, 'Paid'),
        (PENDING, 'Pending'),
        (CANCELED, 'Canceled')
    )

    creator = models.ForeignKey(
        User,
        related_name='orders',
        on_delete=models.CASCADE,
        verbose_name='Пользователь',
        help_text='Пользователь, совершивший заказ'
    )
    status = models.CharField('Статус', choices=STATUS_CHOICES, max_length=10, default=PENDING)
    restaurant = models.ForeignKey(
        'Restaurant',
        related_name='orders',
        on_delete=models.CASCADE,
        verbose_name='Ресторан'
    )

    # В ТЗ мало инфы о операторе. Будем считать, что он указывается сразу при заказе
    # И оператор это User, у которого is_staff=True
    operator = models.ForeignKey(
        User,
        related_name='processing_orders',
        on_delete=models.CASCADE,
        verbose_name='Оператор',
    )
    dishes = models.ManyToManyField('Dish', verbose_name='Блюда')
    creation_datetime = models.DateTimeField('Время создания', auto_now_add=True)

    def __str__(self):
        return f'Order #{self.pk}'

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    @property
    def total_price(self):
        return reduce(lambda price, dish: price + dish.price, self.dishes.all(), 0)

    def serialize(self):
        return {
            'id': self.pk,
            'creator': self.creator.pk,
            'status': self.status,
            'restaurant': self.restaurant.pk,
            'operator': self.operator.pk,
            'dishes': list(self.dishes.values_list('pk', flat=True)),
            'total_price': str(self.total_price),
            'creation_datetime': get_formatted_date(self.creation_datetime)
        }
