from django import forms

from restaurant.common import exceptions
from restaurant.dishes.models import Order


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = (
            'creator',
            'restaurant',
            'operator',
            'dishes',
        )

    def clean_operator(self):
        operator = self.cleaned_data['operator']
        if not operator.is_staff:
            raise exceptions.ValidationError({
                'operator': 'Оператор должен быть is_staff'
            })
        return operator
