from django.conf.urls import url

from restaurant.dishes.views import OrderView, MenuView


urlpatterns = [
    url(r'^orders/$', OrderView.as_view(), name='order'),
    url(r'^menu/$', MenuView.as_view(), name='menu')
]
