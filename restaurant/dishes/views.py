from django.http import JsonResponse

from restaurant.common import exceptions
from restaurant.common.permissions import IsAuthenticated
from restaurant.common.views import APIView
from restaurant.dishes.forms import OrderForm
from restaurant.dishes.models import Category


class OrderView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        data = {
            **request.data,
            'creator': request.user.pk
        }

        form = OrderForm(data)

        if not form.is_valid():
            raise exceptions.ValidationError(form.errors)

        order = form.save()
        return JsonResponse(order.serialize())


class MenuView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        # Красивая сериализация меню

        # Из-за "красоты" происходит множество запросов к БД,
        # если клиенту действительно необходим такой формат, то стоит подумать о других решениях:

        # Производить группировку в питоне или на клиенте.
        # Или же кешеривоть сериализованное дерево.
        # С учетом того, что цена часто изменяется, можно ставить кеш на небольшое время
        return JsonResponse(Category.serialize_whole_tree(), safe=False)
