from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from restaurant.dishes.models import Category, Dish, Order, Restaurant


@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('id', 'city', 'name',)
    search_fields = ('city',)


@admin.register(Category)
class CategoryAdmin(MPTTModelAdmin):
    list_display = ('name', 'parent')
    search_fields = ('name',)


@admin.register(Dish)
class DishAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'price',
        'category',
    )
    search_fields = ('name',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'status',
        'restaurant',
        'operator',
        'total_price',
    )
    list_filter = ('status',)
