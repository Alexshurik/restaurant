from django.contrib.auth import get_user_model
from django.test import Client
from django.urls import reverse
from model_mommy import mommy

from restaurant.common.test_cases import APITestCase
from restaurant.dishes.models import Order, Restaurant, Dish

User = get_user_model()


class OrderTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = mommy.make(User, is_staff=False)
        cls.operator = mommy.make(User, is_staff=True)

        cls.restaurant = mommy.make(Restaurant)

        cls.dish1 = mommy.make(Dish)
        cls.dish2 = mommy.make(Dish)

    def setUp(self):
        self.client.force_login(user=self.user)

    def test_create_order_not_authenticated(self):
        """
        Тестируем на всякий случай,
        что доступ к меню есть только у зарегистрированных пользователей
        """
        client = Client()
        url = reverse('api-v1:order')
        response = client.post(url)
        self.assertEqual(response.status_code, 403)

    def test_create_order(self):
        data = {
            'restaurant': self.restaurant.pk,
            'operator': self.operator.pk,
            'dishes': [self.dish1.pk, self.dish2.pk],
        }

        url = reverse('api-v1:order')
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(
            Order.objects.filter(
                creator=self.user,
                operator=self.operator,
                restaurant=self.restaurant,
                dishes__in=[self.dish2, self.dish1]
            ).exists()
        )

        order = Order.objects.last()
        self.assertEqual(response.json(), order.serialize())

    def test_create_order_wrong_data(self):
        data = {
            'restaurant': 1489,
            'operator': self.operator.pk,
            'dishes': [self.dish1.pk, self.dish2.pk],
        }

        url = reverse('api-v1:order')
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 400)

    def test_create_order_operator_not_staff(self):
        not_staff_user = mommy.make(User, is_staff=False)

        data = {
            'restaurant': self.restaurant.pk,
            'operator': not_staff_user.pk,
            'dishes': [self.dish1.pk, self.dish2.pk],
        }

        url = reverse('api-v1:order')
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 400)
