from django.contrib.auth import get_user_model
from django.test import testcases, Client
from django.urls import reverse
from model_mommy import mommy

from restaurant.common.test_cases import APITestCase
from restaurant.dishes.models import Order, Restaurant, Dish, Category

User = get_user_model()


class MenuTests(APITestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        cls.user = mommy.make(User, is_staff=False)

        # Создадим несколько категорий
        cls.root_category_1 = mommy.make(Category, name='Еда')
        cls.root_category_2 = mommy.make(Category, name='Напитки')

        cls.level_2_category1 = mommy.make(Category, name='Чипсы', parent=cls.root_category_1)
        cls.level_2_category2 = mommy.make(Category, name='Соки', parent=cls.root_category_2)

        # Блюда в этих категориях
        cls.dish1 = mommy.make(Dish, name='Джей севен', category=cls.level_2_category2)
        cls.dish2 = mommy.make(Dish, name='Моя семья', category=cls.level_2_category2)

        cls.dish3 = mommy.make(Dish, name='Лейс', category=cls.level_2_category1)
        cls.dish4 = mommy.make(Dish, name='Принглс', category=cls.level_2_category1)

    def test_get_menu_not_authenticated(self):
        """
        Тестируем на всякий случай,
        что доступ к меню есть только у зарегистрированных пользователей
        """
        url = reverse('api-v1:menu')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_get_menu(self):
        client = Client()
        client.force_login(user=self.user)

        url = reverse('api-v1:menu')
        response = client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), Category.serialize_whole_tree())
