import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

DEBUG = False

ROOT_URLCONF = 'restaurant.urls'

WSGI_APPLICATION = 'restaurant.wsgi.application'

ADMINS = [
    ('restaurant', 'restaurant@mail.ru'),
]

MEDIA_URL = '/media/'
MEDIA_ROOT = '/var/restaurant/media/'

STATIC_URL = '/static/'
STATIC_ROOT = '/var/restaurant/static/'


from .installed_apps import *
from .internationalization import *
from .middleware import *
from .password_validation import *
from .templates import *

# see local.example
try:
    from .local import *
except Exception:
    pass
