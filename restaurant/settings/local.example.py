ALLOWED_HOSTS = [
    '*'
]

SECRET_KEY = 'a=h0xdw-!(o)r*&i6xmj=^7s9*s5+)9a7f@8d4xuwr7vckf+d$'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'restaurant',
        'USER': 'restaurant',
        'PASSWORD': 'restaurant',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
