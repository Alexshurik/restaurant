from . import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'restaurant',
        'USER': 'restaurant',
        'PASSWORD': 'restaurant',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

SECRET_KEY = 'a=h0xdw-!(o)r*&i6xmj=^7s9*s5+)9a7f@8d4xuwr7vckf+d$'

ALLOWED_HOSTS = [
    '*'
]

CORS_ORIGIN_ALLOW_ALL = True
