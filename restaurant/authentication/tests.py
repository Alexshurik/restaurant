import json

from django.contrib.auth import get_user_model
from django.urls import reverse

from restaurant.authentication.models import Token
from restaurant.common.test_cases import APITestCase

User = get_user_model()


class AuthTests(APITestCase):
    def test_login(self):
        credentials = {
            'username': 'test_user',
            'password': 'test_password'
        }

        user = User.objects.create_user(**credentials)

        url = reverse('api-v1:login')
        response = self.client.post(url, credentials)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(Token.objects.filter(user=user).exists())
        self.assertTrue(Token.objects.filter(key=response.json()['token']).exists())

    def test_login_wrong_credentials(self):
        credentials = {
            'username': 'test_user',
            'password': 'test_password'
        }

        user = User.objects.create_user(**credentials)

        url = reverse('api-v1:login')
        response = self.client.post(url, {'login': 'wrong', 'password': 'wrong'})

        self.assertEqual(response.status_code, 400)

    def test_register(self):
        credentials = {
            'username': 'test_user',
            'password': 'test_password'
        }

        url = reverse('api-v1:register')
        response = self.client.post(url, credentials)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(Token.objects.filter(key=response.json()['token']).exists())

    def test_register_username_exists(self):
        credentials = {
            'username': 'test_user',
            'password': 'test_password'
        }

        user = User.objects.create_user(**credentials)

        url = reverse('api-v1:register')
        response = self.client.post(url, credentials)

        self.assertEqual(response.status_code, 400)
