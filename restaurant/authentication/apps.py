from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'restaurant.authentication'
