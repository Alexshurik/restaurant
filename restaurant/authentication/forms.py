from django import forms


class AuthForm(forms.Form):  # Используем формы для валидации
    username = forms.CharField()
    password = forms.CharField()
