import binascii

import os
from django.contrib.auth import get_user_model
from django.db import models


User = get_user_model()


class Token(models.Model):
    key = models.CharField('Ключ', max_length=40, primary_key=True, db_index=True)
    user = models.OneToOneField(
        User,
        related_name='token',
        on_delete=models.CASCADE,
        verbose_name='Пользователь'
    )
    creation_datetime = models.DateTimeField(
        'Время создания',
        auto_now_add=True
    )

    class Meta:
        verbose_name = 'Токен'
        verbose_name_plural = 'Токены'

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key
