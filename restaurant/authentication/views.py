from django.contrib.auth import get_user_model, authenticate
from django.http import JsonResponse

from restaurant.authentication.forms import AuthForm
from restaurant.authentication.models import Token
from restaurant.common import exceptions
from restaurant.common.views import APIView


User = get_user_model()


class RegisterView(APIView):
    def post(self, request):
        form = AuthForm(request.data)

        if not form.is_valid():
            raise exceptions.ValidationError(form.errors)

        if User.objects.filter(username=request.data['username']).exists():
            raise exceptions.ValidationError({
                'username': 'Пользователь с таким именем уже существует'
            })

        user = User.objects.create_user(
            username=request.data['username'],
            password=request.data['password']
        )

        token = Token.objects.create(user=user)
        return JsonResponse({
            'token': token.key
        })


class LoginView(APIView):
    def post(self, request):
        form = AuthForm(request.data)

        if not form.is_valid():
            raise exceptions.ValidationError(form.errors)

        user = authenticate(
            username=request.data['username'],
            password=request.data['password']
        )

        if not user:
            raise exceptions.ValidationError({
                'error': 'Неверный username или пароль'
            })

        token, created = Token.objects.get_or_create(user=user)
        return JsonResponse({
            'token': token.key
        })
