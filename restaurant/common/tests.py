from django.contrib.auth import get_user_model
from django.test import testcases, Client
from django.urls import reverse
from model_mommy import mommy

from restaurant.authentication.models import Token

User = get_user_model()


class TokenAuthenticationTests(testcases.TestCase):
    """
    Проверяем аутентификацию по токену на примере OrderView

    По-хорошему, надо было в тестах создать для этого отдельную вьюху и проверять на ней,
    но мне сейчас лень
    """

    @classmethod
    def setUpTestData(cls):
        cls.user = mommy.make(User)

    def test_not_authenticated(self):
        url = reverse('api-v1:order')
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 403)

    def test_wrong_token_value(self):
        client = Client(HTTP_AUTHORIZATION='Token wrongtoken!!!')

        url = reverse('api-v1:order')
        response = client.post(url, {})
        self.assertEqual(response.status_code, 401)

    def test_wrong_token_keyword(self):
        client = Client(HTTP_AUTHORIZATION='EtoNeToken wrongtoken!!!')

        url = reverse('api-v1:order')
        response = client.post(url, {})
        self.assertEqual(response.status_code, 403)

    def test_authenticated(self):
        token = Token.objects.create(user=self.user)

        client = Client(HTTP_AUTHORIZATION=f'Token {token.key}')

        url = reverse('api-v1:order')
        response = client.post(url, {})
        self.assertEqual(response.status_code, 400)  # Мы не ввели нужны данные для вьюхи, но прошли аутентификацию
