class BasePermission:
    def has_permission(self, request):
        return True


class IsAuthenticated(BasePermission):
    def has_permission(self, request):
        return request.user and request.user.is_authenticated
