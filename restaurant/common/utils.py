def get_formatted_date(date):
    """
    Приводит дату к формату, в котором ее выдает стандартный сериалайзер DRF
    """
    value = date.isoformat()
    if value.endswith('+00:00'):
        value = value[:-6] + 'Z'
    return value
