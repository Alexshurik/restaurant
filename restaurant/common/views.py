import json

from django.contrib.auth.models import AnonymousUser
from django.http import JsonResponse
from django.views import View

from restaurant.common import exceptions
from restaurant.common.authentication import TokenAuthentication


class APIView(View):
    permission_classes = []
    authentication_class = TokenAuthentication

    def handle_exception(self, exc):
        if not isinstance(exc, exceptions.exceptions):
            raise

        if isinstance(exc.msg, dict):
            response_data = exc.msg
        else:
            response_data = {
                'error': exc.msg
            }
        return JsonResponse(response_data, status=exc.status_code)

    def authenticate(self, request):
        if self.authentication_class:
            user, token = self.authentication_class().authenticate(request)
            request.user = user
        else:
            request.user = None

    def check_permissions(self, request):
        for permission in self.permission_classes:
            if not permission().has_permission(request):
                raise exceptions.PermissionDenied()

    def parse_json(self, request):
        try:
            if request.body:
                request.data = json.loads(request.body)
            else:
                request.data = {}
        except ValueError as exc:
            raise exceptions.ValidationError({
                'error': 'Невозможно распарсить json'
            })

    def dispatch(self, request, *args, **kwargs):
        try:
            # Чтобы можно было делать force_login() в тестах
            if not hasattr(request, 'user') or not request.user or isinstance(request.user, AnonymousUser):
                self.authenticate(request)
            self.check_permissions(request)
            self.parse_json(request)

            response = super().dispatch(request, *args, **kwargs)
        except Exception as exc:
            response = self.handle_exception(exc)
        return response
