from django.utils.six import text_type

from restaurant.authentication.models import Token
from restaurant.common import exceptions


HTTP_HEADER_ENCODING = 'iso-8859-1'


def get_authorization_header(request):
    """
    Функция-помощник, чтобы корректно вытащить авторизационный заголовок из HTTP-запроса
    """
    auth = request.META.get('HTTP_AUTHORIZATION', b'')
    if isinstance(auth, text_type):
        # Work around django test client oddness
        auth = auth.encode(HTTP_HEADER_ENCODING)
    return auth


class BaseAuthentication:
    def authenticate(self, request):
        raise NotImplementedError('Необходимо переопределить .authenticate()')


class TokenAuthentication(BaseAuthentication):
    """
    Авторизация по токену

    Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """

    keyword = 'Token'

    def authenticate(self, request):
        auth = get_authorization_header(request).split()
        # auth = request.META.get('HTTP_AUTHORIZATION', '').split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None, None

        if len(auth) == 1:
            msg = 'Неверный заговок токена'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Токен не должен содержать пробелы'
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = 'Токен содержит недопустимые символы'
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, key):
        try:
            token = Token.objects.select_related('user').get(key=key)
        except Token.DoesNotExist:
            raise exceptions.AuthenticationFailed('Неверный токен')

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed('Аккаунт пользователя деактивирован')

        return token.user, token
