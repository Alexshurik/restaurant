class APIException(Exception):
    status_code = 500
    default_msg = 'Ошибка на сервере'

    def __init__(self, msg=None):
        self.msg = msg or self.default_msg


class ValidationError(APIException):
    status_code = 400
    default_msg = 'Неверные введенные данные'


class AuthenticationFailed(APIException):
    status_code = 401
    default_msg = 'Неверные данные аутентификации'


class NotAuthenticated(APIException):
    status_code = 401
    default_msg = 'Данные для аутентификации не были предоставлены'


class PermissionDenied(APIException):
    status_code = 403
    default_msg = 'У вас не достаточно прав для данного действия'


exceptions = (
    APIException,
    AuthenticationFailed,
    NotAuthenticated,
    PermissionDenied,
    ValidationError,
)
