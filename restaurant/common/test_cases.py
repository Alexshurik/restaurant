import json

from django.test import Client, testcases


class JsonClient(Client):
    """
    Тестовый клиент, который кодирует данные в json
    """
    content_type = 'application/json'

    def post(self, path, data=None, content_type=content_type,
             follow=False, secure=False, **extra):
        if data:
            data = json.dumps(data)
        return super().post(path, data=data, content_type=content_type,
                            follow=False, secure=False, **extra)

    # ToDO: при желании можно так же переопределить put и patch методы


class APITestCase(testcases.TestCase):
    client_class = JsonClient
