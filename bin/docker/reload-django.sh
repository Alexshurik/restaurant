#!/bin/bash

CONTAINER='django';

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

docker exec -it ${CONTAINER} python /home/app/manage.py collectstatic --noinput;
docker exec -it ${CONTAINER} python /home/app/manage.py migrate;
echo 'Restarting container'
docker restart ${CONTAINER}
